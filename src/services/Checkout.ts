import { CustomerData } from "../interfaces/CustomerData"
import { PaymentData } from "../interfaces/PaymentData"
import { FoodData } from "../interfaces/FoodData"
import { Customer, Order, PrismaClient } from "@prisma/client"
import Payment from "./Payment"

export default class Checkout {
  private prisma: PrismaClient

  constructor() {
    this.prisma = new PrismaClient()
  }

  async process(
    cart: FoodData[],
    customer: CustomerData,
    payment: PaymentData
   ): Promise<{ id: number; transactionId: string; status: string }> {
    
    const foods = await this.prisma.food.findMany({
      where: { id: { in: cart.map((food) => food.id),
      },
    },    
  })
  
  const foodsInCart = foods.map<FoodData>((food) => ({
    ...food,
    price: Number(food.price),
    quantity: cart.find((item) => item.id === food.id)?.quantity!,
    subTotal: cart.find((item) => item.id === food.id)?.quantity! * Number(food.price),
  }))
  
  const customerCreated = await this.createCustomer(customer)
  //console.log(`customerCreated`, customerCreated)

  let orderCreated = await this.createOrder(foodsInCart, customerCreated)

  const {transactionId, status } = await new Payment().process(orderCreated, customerCreated, payment)
  
  orderCreated = await this.prisma.order.update({
    where: { id: orderCreated.id },
    data: {
      transactionId,
      status,
    },
  })

  return {
    id: orderCreated.id,
    transactionId: orderCreated.transactionId!,
    status: orderCreated.status,
  }
}  
  
private async createCustomer(customer: CustomerData): Promise<Customer> {
  const customerCreated = await this.prisma.customer.upsert({
      where: { email: customer.email},
      update: customer,
      create: customer,
    })
    
    return customerCreated

   }

private async createOrder(foodsInCart: FoodData[], customer: Customer):
  Promise<Order> {  
  const total = foodsInCart.reduce((acc, food) => acc + food.subTotal,0)
  
  const orderCreated = await this.prisma.order.create({
    data: {
      total,
      customer: {
        connect: { id: customer.id },
      },
      orderItems: {
        createMany: {
          data: foodsInCart.map((food) => ({
            foodId: food.id,
            quantity: food.quantity,
            subTotal: food.subTotal,
          }))
        }
      }
    },
    include: {
      customer: true, orderItems: { include: {food: true }},

    }
 })

 return orderCreated

}
}