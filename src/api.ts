import axios from "axios"
import dotenv from "dotenv"

dotenv.config()

export const api = axios.create({
  baseURL: process.env.ASAAS_API_SANDBOX,
  headers: {
    access_token: process.env.ASAAS_TOKEN_SANDBOX,
  },
})